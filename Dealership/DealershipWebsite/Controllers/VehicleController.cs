﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using DealershipWebsite.Models;
using DealershipWebsite.ViewModels;
using PagedList;

namespace DealershipWebsite.Controllers
{
    public class VehicleController : Controller
    {
        private DealershipDatabase db = new DealershipDatabase();

        // GET: Vehicles
        public ActionResult Index(
            string search,
            int? minYear,
            int? maxYear,
            string sortBy,
            decimal? minPrice = 2500,
            decimal? maxPrice = 50000,
            int? page = 1,
            int? itemsPerPage = 5)
        {
            var vehicles =
                from v in db.Vehicles
                orderby v.VehicleMake, v.VehicleModel, v.VehicleYear
                select new VehicleInfo
                {
                    VehicleVin = v.VehicleVin,
                    VehicleMake = v.VehicleMake,
                    VehicleModel = v.VehicleModel,
                    VehicleYear = v.VehicleYear,
                    VehicleColor = v.VehicleColor,
                    VehiclePrice = v.VehiclePrice,
                    Image = v.Image
                };

            // Search
            if (!string.IsNullOrWhiteSpace(search))
            {
                vehicles = vehicles.Where(v => v.VehicleMake == search || v.VehicleModel == search || Convert.ToString(v.VehicleYear) == search);
            }
            if (minYear != null)
            {
                vehicles = vehicles.Where(v => v.VehicleYear >= minYear);
            }
            if (maxYear != null)
            {
                vehicles = vehicles.Where(v => v.VehicleYear == maxYear);
            }

            // Sorting
            switch (sortBy)
            {
                default:
                    vehicles.OrderBy(v => v.VehicleMake);
                    break;
                case "Make":
                    vehicles.OrderBy(v => v.VehicleMake);
                    break;
                case "Model":
                    vehicles.OrderBy(v => v.VehicleModel);
                    break;
                case "Year":
                    vehicles.OrderBy(v => v.VehicleYear);
                    break;
                case "Price":
                    vehicles.OrderBy(v => v.VehiclePrice);
                    break;
            }

            //if (minPrice != null && maxPrice != null)
            //{
            //    vehicles = vehicles.Where(v => v.VehiclePrice >= minPrice && v.VehiclePrice <= maxPrice);
            //}
            if (maxPrice != null)
            {
                vehicles = vehicles.Where(v => v.VehiclePrice <= maxPrice);
            }
            if (minPrice != null)
            {
                vehicles = vehicles.Where(v => v.VehiclePrice >= minPrice);
            }

            var model = new VehicleSearchResults
            {
                Search = search,
                MinYear = minYear,
                MaxYear = maxYear,
                MinPrice = minPrice,
                MaxPrice = maxPrice,
                SortBy = sortBy,
                Results = vehicles.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };

            return View("Index", model);
        }

        // GET: Vehicles/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        private void PopulateDropDowns()
        {
            string folderPath = Server.MapPath("~/Content/Images");
            ViewBag.Images =
                Directory.EnumerateFiles(folderPath)
                         .Select(x => Path.GetFileName(x));
        }

        // GET: Vehicles/Create
        public ActionResult Create()
        {
            PopulateDropDowns();
            return View();
        }

        // POST: Vehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VehicleVin,VehicleMake,VehicleModel,VehicleYear,VehicleColor,VehiclePrice,Image")] Vehicle vehicle,
            HttpPostedFileBase imageUpload)
        {
            if (imageUpload != null &&
                ValidateImage("Image", imageUpload))
            {
                UploadVehicleImage(vehicle, imageUpload);
            }

            if (ModelState.IsValid)
            {
                db.Vehicles.Add(vehicle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            PopulateDropDowns();
            return View(vehicle);
        }

        private bool ValidateImage(string name, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError(name, "Please select a file.");
                return false;
            }
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(name, "File size was zero.");
                return false;
            }
            if (file.ContentLength >= Constants.MAX_UPLOAD_FILE_SIZE)
            {
                ModelState.AddModelError(name, "File size was to big.");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!Constants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(name, $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        private void UploadVehicleImage(Vehicle vehicle, HttpPostedFileBase file)
        {
            // Resize the image
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > 350 || img.Height > 600)
            {
                img.Resize(150, 300);
            }

            // Save the image
            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            string fileName = $"VEHICLE_{vehicle.VehicleVin}{fileExtension}";
            img.Save(Constants.IMAGES_FOLDER + fileName);

            // Save the thumbnails
            img.Resize(100, 100);
            img.Save(Constants.IMAGES_FOLDER + Constants.THUMBNAILS + fileName);

            // Save the name of the image to the database
            vehicle.Image = fileName;
        }

        // GET: Vehicles/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // POST: Vehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VehicleVin,VehicleMake,VehicleModel,VehicleYear,VehicleColor,VehiclePrice,Image")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vehicle);
        }

        // GET: Vehicles/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // POST: Vehicles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Vehicle vehicle = db.Vehicles.Find(id);
            db.Vehicles.Remove(vehicle);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
