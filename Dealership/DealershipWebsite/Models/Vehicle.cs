﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DealershipWebsite.Models
{
    [Table("Vehicle")]
    public class Vehicle
    {
        [Key]
        [Display (Name = "Vin Number")]
        public string VehicleVin { get; set; }

        [Display(Name = "Vehicle Make")]
        [Required]
        public string VehicleMake { get; set; }

        [Display(Name = "Vehicle Model")]
        [Required]
        public string VehicleModel { get; set; }

        [Display(Name = "Vehicle Year")]
        [Required]
        public int VehicleYear { get; set; }

        [Display(Name = "Color")]
        [Required]
        public string VehicleColor { get; set; }

        [Display(Name = "Vehicle Price")]
        [Required]
        public decimal VehiclePrice { get; set; }

        [Display(Name = "Images")]
        [DataType(DataType.ImageUrl)]
        public string Image { get; set; }
    }
}