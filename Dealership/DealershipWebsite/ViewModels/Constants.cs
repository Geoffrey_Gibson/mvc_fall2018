﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealershipWebsite.ViewModels
{
    public class Constants
    {
        public const int MAX_UPLOAD_FILE_SIZE = 10 * 1024 * 1024;
        public const string IMAGES_FOLDER = "~/Content/Images/";
        public const string THUMBNAILS = "Thumbnails/";
        
        public static readonly string[] ALLOWED_IMAGE_EXTENSIONS =
        {
            ".jpg", ".jpeg", ".png", ".gif"
        };
    }
}