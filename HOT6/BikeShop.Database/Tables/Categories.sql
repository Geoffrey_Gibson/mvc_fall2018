﻿CREATE TABLE [dbo].[Categories]
(
    [CategoryId] TINYINT NOT NULL,
    [CategoryName] VARCHAR(20) NOT NULL,

    PRIMARY KEY ([CategoryId])
)
GO
