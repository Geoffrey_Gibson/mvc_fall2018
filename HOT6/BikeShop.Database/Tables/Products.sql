﻿CREATE TABLE [dbo].[Products]
(
    [ProductId] BIGINT IDENTITY(1,1) NOT NULL,
    [ProductName] NVARCHAR(40) NOT NULL,
    [RetailPrice] DECIMAL(10,2) NOT NULL,
    [QuantityOnHand] SMALLINT NOT NULL,
    [CategoryId] TINYINT NOT NULL,

    PRIMARY KEY ([ProductId]),
    FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Categories] ([CategoryId])
)
