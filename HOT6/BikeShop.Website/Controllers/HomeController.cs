﻿using BikeShop.Website.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BikeShop.Website.Controllers
{
    public class HomeController : Controller
    {
        private BikeShopDatabase db = new BikeShopDatabase();

        public ActionResult Index()
        {
            var categories =
                from c in db.Categories
                orderby c.CategoryName
                select c;

            return View("Index", categories.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                db = null;
            }
            base.Dispose(disposing);
        }
    }
}