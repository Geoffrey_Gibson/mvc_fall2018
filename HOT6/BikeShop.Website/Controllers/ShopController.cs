﻿using BikeShop.Website.Models;
using BikeShop.Website.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BikeShop.Website.Controllers
{
    public class ShopController : Controller
    {
        private BikeShopDatabase db = new BikeShopDatabase();

        // GET: Category
        public ActionResult Index(
            string search,
            byte? categoryId,
            int? minPrice,
            int? maxPrice,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            var products =
                from p in db.Products
                join c in db.Categories on p.CategoryId equals c.CategoryId
                orderby p.ProductId, c.CategoryId
                select new BikeShopInfo
                {
                    ProductId = p.ProductId,
                    ProductName = p.ProductName,
                    RetailPrice = p.RetailPrice,
                    Quantity = p.QuantityOnHand,
                    CategoryId = c.CategoryId,
                    CategoryName = c.CategoryName
                };

            if (!string.IsNullOrEmpty(search))
            {
                products = products.Where(x => x.ProductName.Contains(search) || x.CategoryName.Contains(search));
            }
            if (categoryId != null)
            {
                products = products.Where(x => x.CategoryId == categoryId);
            }
            if (minPrice != null)
            {
                products = products.Where(x => x.RetailPrice >= minPrice);
            }
            if (maxPrice != null)
            {
                products = products.Where(x => x.RetailPrice <= maxPrice);
            }

            switch (sortBy)
            {
                default:
                    products = products.OrderBy(e => e.ProductId);
                    break;
                case "Category #":
                    products = products.OrderBy(e => e.CategoryId);
                    break;
                case "Product #":
                    products = products.OrderBy(e => e.ProductId);
                    break;
                case "Price":
                    products = products.OrderBy(e => e.RetailPrice);
                    break;
                case "Product Name":
                    products = products.OrderBy(e => e.ProductName);
                    break;
            }



            var model = new BikeShopSearchResults
            {
                Search = search,
                CategoryId = categoryId,
                MinPrice = minPrice,
                MaxPrice = maxPrice,
                SortBy = sortBy,
                Categories = db.Categories.OrderBy(c => c.CategoryName).ToList(),
                Results = products.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };

            ViewBag.Search = search;
            ViewBag.CategoryId = categoryId;
            ViewBag.MinPrice = minPrice;
            ViewBag.MaxPrice = maxPrice;
            ViewBag.SortBy = sortBy;
            ViewBag.Genres = db.Categories.OrderBy(c => c.CategoryName).ToList();

            return View("Index", model);
        }
    }
}