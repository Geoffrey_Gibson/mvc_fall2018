﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeShop.Website.Models
{
    public class Category
    {
        public byte CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}