﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeShop.Website.Models
{
    public class Product
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal RetailPrice { get; set; }
        public short QuantityOnHand { get; set; }

        public byte CategoryId { get; set; }
    }
}