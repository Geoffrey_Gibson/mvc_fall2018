﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeShop.Website.ViewModels
{
    public class BikeShopInfo
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal RetailPrice { get; set; }
        public short Quantity { get; set; }
        public byte CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}