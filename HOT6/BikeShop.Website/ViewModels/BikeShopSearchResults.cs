﻿using BikeShop.Website.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeShop.Website.ViewModels
{
    public class BikeShopSearchResults
    {
        public string Search { get; set; }
        public byte? CategoryId { get; set; }
        public int? MinPrice { get; set; }
        public int? MaxPrice { get; set; }
        public string SortBy { get; set; }

        public IEnumerable<Category> Categories { get; set; }
        public IPagedList<BikeShopInfo> Results { get; set; }
    }
}