﻿insert into [Categories] ([CategoryId], [CategoryName])
values
(6, 'Wheels'),
(5, 'Components'),
(4, 'Clothing'),
(3, 'Car Racks'),
(2, 'Bikes'),
(1, 'Accessories')
;

SET IDENTITY_INSERT [dbo].[products] ON;

insert into [Products] ([ProductId], [ProductName], [RetailPrice], [QuantityOnHand], [CategoryId])
values
(1, N'Trek 9000 Mountain Bike', CAST(1200.00 AS DECIMAL(10,2)), 6, 2),
(2, N'Eagle FS-3 Mountain Bike', CAST(1800.00 AS DECIMAL(10,2)), 8, 2),
(3, N'Dog Ear Cyclecomputer', CAST(75.00 AS DECIMAL(10,2)), 20, 1),
(4, N'Victoria Pro All Weather Tires', CAST(54.95 AS DECIMAL(10,2)), 20, 4),
(5, N'Dog Ear Helmet Mount Mirrors', CAST(7.45 AS DECIMAL(10,2)), 12, 1),
(6, N'Viscount Mountain Bike', CAST(635.00 AS DECIMAL(10,2)), 5, 2),
(7, N'Viscount C-500 Wireless Bike Computer', CAST(49.00 AS DECIMAL(10,2)), 30, 1),
(8, N'Kryptonite Advanced 2000 U-Lock', CAST(50.00 AS DECIMAL(10,2)), 20, 1),
(9, N'Nikoma Lok-Tight U-Lock', CAST(33.00 AS DECIMAL(10,2)), 12, 1),
(10, N'Viscount Microshell Helmet', CAST(36.00 AS DECIMAL(10,2)), 20, 1),
(11, N'GT RTS-2 Mountain Bike', CAST(1650.00 AS DECIMAL(10,2)), 5, 2),
(12, N'Shinoman 105 SC Brakes', CAST(23.50 AS DECIMAL(10,2)), 16, 4),
(13, N'Shinoman Dura-Ace Headset', CAST(67.50 AS DECIMAL(10,2)), 20, 4),
(14, N'Eagle SA-120 Clipless Pedals', CAST(139.95 AS DECIMAL(10,2)), 20, 4),
(15, N'ProFormance Toe-Klips 2G', CAST(4.99 AS DECIMAL(10,2)), 40, 4),
(16, N'ProFormance ATB All-Terrain Pedal', CAST(28.00 AS DECIMAL(10,2)), 40, 4),
(17, N'Shinoman Deluxe TX-30 Pedal', CAST(45.00 AS DECIMAL(10,2)), 60, 4),
(18, N'Viscount CardioSport Sport Watch', CAST(179.00 AS DECIMAL(10,2)), 12, 1),
(19, N'Viscount Tru-Beat Heart Transmitter', CAST(47.00 AS DECIMAL(10,2)), 20, 1),
(20, N'Dog Ear Monster Grip Gloves', CAST(15.00 AS DECIMAL(10,2)), 30, 1),
(21, N'Dog Ear Aero-Flow Floor Pump', CAST(55.00 AS DECIMAL(10,2)), 25, 1),
(22, N'Pro-Sport ''Dillo Shades', CAST(82.00 AS DECIMAL(10,2)), 18, 1),
(23, N'Ultra-Pro Rain Jacket', CAST(85.00 AS DECIMAL(10,2)), 30, 3),
(24, N'StaDry Cycling Pants', CAST(69.00 AS DECIMAL(10,2)), 22, 3),
(25, N'King Cobra Helmet', CAST(139.00 AS DECIMAL(10,2)), 30, 1),
(26, N'Glide-O-Matic Cycling Helmet', CAST(125.00 AS DECIMAL(10,2)), 24, 1),
(27, N'X-Pro All Weather Tires', CAST(24.00 AS DECIMAL(10,2)), 20, 6),
(28, N'Turbo Twin Tires', CAST(29.00 AS DECIMAL(10,2)), 18, 6),
(29, N'Ultra-2K Competition Tire', CAST(34.00 AS DECIMAL(10,2)), 22, 6),
(30, N'Clear Shade 85-T Glasses', CAST(45.00 AS DECIMAL(10,2)), 14, 1),
(31, N'True Grip Competition Gloves', CAST(22.00 AS DECIMAL(10,2)), 20, 1),
(32, N'Kool-Breeze Rocket Top Jersey', CAST(32.00 AS DECIMAL(10,2)), 12, 3),
(33, N'Wonder Wool Cycle Socks', CAST(19.00 AS DECIMAL(10,2)), 30, 3),
(34, N'TransPort Bicycle Rack', CAST(27.00 AS DECIMAL(10,2)), 14, 1),
(35, N'HP Deluxe Panniers', CAST(39.00 AS DECIMAL(10,2)), 10, 1),
(36, N'Cosmic Elite Road Warrior Wheels', CAST(165.00 AS DECIMAL(10,2)), 22, 4),
(37, N'AeroFlo ATB Wheels', CAST(189.00 AS DECIMAL(10,2)), 40, 4),
(38, N'Cycle-Doc Pro Repair Stand', CAST(166.00 AS DECIMAL(10,2)), 12, 1),
(39, N'Road Warrior Hitch Pack', CAST(175.00 AS DECIMAL(10,2)), 6, 5),
(40, N'Ultimate Export 2G Car Rack', CAST(180.00 AS DECIMAL(10,2)), 8, 5)
;

SET IDENTITY_INSERT [dbo].[products] OFF;
