﻿CREATE TABLE [dbo].[Sales]
(
	[SaleId] UNIQUEIDENTIFIER NOT NULL,
	[Discount] DECIMAL(3,0) NOT NULL,
	[StartDate] DATE NOT NULL,
	[EndDate] DATE NOT NULL,
	[CategoryId] TINYINT NOT NULL,

	[ProductId] BIGINT NOT NULL,

	PRIMARY KEY ([SaleId]),
	FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([ProductId]),
	FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Categories] ([CategoryId])
)
