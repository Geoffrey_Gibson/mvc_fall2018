﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BikeShop.Website.Models
{
    public class Category
    {
        [Key]
        public byte CategoryId { get; set; }
        [StringLength(40, ErrorMessage = "Category name is too long.")]
        [MinLength(3, ErrorMessage = "Category name is too short")]
        [Required(ErrorMessage = "Category must have a name")]
        public string CategoryName { get; set; }

        [ForeignKey("CategoryId")]
        public virtual ICollection<Product> Products { get; set; }
    }
}