﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BikeShop.Website.Models
{
    public class Product
    {
        [Key]
        [Display(Name = "#")]
        public long ProductId { get; set; }
        

        [StringLength(50, ErrorMessage = "Product name is too long.")]
        [MinLength(3, ErrorMessage = "Product name is too short")]
        [Required(ErrorMessage = "Product must have a name")]
        [Display(Name = "Name")]
        public string ProductName { get; set; }
        [Required(ErrorMessage = "Product must have a price")]
        [Display(Name = "Price")]
        public decimal RetailPrice { get; set; }
        [Required(ErrorMessage = "At least 1 product")]
        [Display(Name = "Quantity")]
        public short QuantityOnHand { get; set; }
        [Display(Name = "Category")]
        public byte CategoryId { get; set; }

        // Navigation
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
        //[ForeignKey("SaleId")]
        //public virtual ICollection<Sale> Sale { get; set; }
    }
}