﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BikeShop.Website.Models
{
    public class Sale
    {
        [Key]
        public Guid SaleId { get; set; }

        [Display(Name = "#")]
        public long ProductId { get; set; }

        [Required]
        [Range(0.00d, 100.00d)]
        [Display(Name = "Discount %")]
        public decimal Discount { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Category")]
        public byte CategoryId { get; set; }

        // Navigation
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }
}