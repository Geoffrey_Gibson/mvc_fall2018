﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BikeShop.Website.Startup))]
namespace BikeShop.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
