﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeShop.Website.ViewModels
{
    public class ProductOnSale
    {
        public Guid SaleId { get; set; }
        public long ProductId { get; set; }
        public byte CategoryId { get; set; }
    }
}