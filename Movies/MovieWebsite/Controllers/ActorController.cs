﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using MovieWebsite.Models;
using MovieWebsite.ViewModels;

namespace MovieWebsite.Controllers
{
    public class ActorController : Controller
    {
        private MovieDatabase db = new MovieDatabase();

        // GET: Actor
        public ActionResult Index()
        {
            var actors = db.Actors.Include(m => m.movie);
            return View(actors.ToList());
        }

        // GET: Actor/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actors.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        // GET: Actor/Create
        public ActionResult Create()
        {
            ViewBag.MovieId = new SelectList(db.Movies, "MovieId", "MovieName");
            return View();
        }

        private void SaveActorImage(Actor actor, HttpPostedFileBase file)
        {
            //string fileExtension = Path.GetExtension(file.FileName).ToLower();
            //string fileName = $"MOVIE_{movie.MovieId}{fileExtension}";
            string fileName = file.FileName;

            // Resize the image
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > 300 || img.Height > 450)
            {
                img.Resize(300, 450);
            }
            img.Save(Constants.ACTOR_FOLDER + fileName);

            // Save the thumbnails
            img.Resize(120, 100);
            img.Save(Constants.ACTOR_FOLDER + Constants.ACTOR_THUMBNAILS + fileName);

            // Save the name of the image to the database
            actor.ActorImage = fileName;
        }

        public bool ValidateFile(HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError("myFile", "Please select a file.");
                return false;
            }
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError("myFile", "Invalid File");
                return false;
            }
            if (file.ContentLength >= 5000000)
            {
                ModelState.AddModelError("myFile", "File size was to big.");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!Constants.FILE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError("myFile", $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        // POST: Actor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ActorId,ActorName,ActorRole,ActorImage,MovieId")]
        Actor actor, 
        HttpPostedFileBase imageUpload)
        {
            if (imageUpload != null && ValidateFile(imageUpload))
            {
                SaveActorImage(actor, imageUpload);
            }

            if (ModelState.IsValid)
            {
                db.Actors.Add(actor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MovieId = new SelectList(db.Movies, "MovieId", "MovieName", actor.MovieId);
            return View(actor);
        }

        // GET: Actor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actors.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            ViewBag.MovieId = new SelectList(db.Movies, "MovieId", "MovieName", actor.MovieId);
            return View(actor);
        }

        // POST: Actor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ActorId,ActorName,ActorRole,ActorImage,MovieId")]
        Actor actor, 
            HttpPostedFileBase imageUpload)
        {
            if (imageUpload != null &&
                ValidateFile(imageUpload))
            {
                SaveActorImage(actor, imageUpload);
            }

            if (ModelState.IsValid)
            {
                db.Entry(actor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MovieId = new SelectList(db.Movies, "MovieId", "MovieName", actor.MovieId);
            return View(actor);
        }

        // GET: Actor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actors.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        // POST: Actor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Actor actor = db.Actors.Find(id);
            db.Actors.Remove(actor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
