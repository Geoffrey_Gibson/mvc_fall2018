﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovieWebsite.Models;
using PagedList;
using MovieWebsite.ViewModels;
using System.IO;
using System.Web.Helpers;

namespace MovieWebsite.Controllers
{
    public class MovieController : Controller
    {
        private MovieDatabase db = new MovieDatabase();

        // GET: ScaffoldMovie
        public ActionResult Index(
            string search,
            int? genreId,
            int? minYear,
            int? maxYear,
            string sortBy,
            int? page,
            int? ItemsPerPage)
        {
            IQueryable<Movie> movies = db.Movies.Include("Genre");

            //Filter Results
            if (genreId != null)
            {
                movies = movies.Where(m => m.GenreId == genreId);
            }
            if (minYear != null)
            {
                movies = movies.Where(m => m.ReleasedYear >= minYear);
            }
            if (maxYear != null)
            {
                movies = movies.Where(m => m.ReleasedYear <= maxYear);
            }

            // Search by keyword
            if (!String.IsNullOrWhiteSpace(search))
            {
                movies = movies.Where(
                    m => m.MovieName.Contains(search) ||
                         m.MovieDescription.Contains(search) ||
                         m.ReleasedYear.ToString().Contains(search) ||
                         m.Genre.GenreName.Contains(search)
                    );
            }

            // Sort Results
            switch (sortBy)
            {
                default:
                case "name":
                    movies = movies.OrderBy(m => m.MovieName);
                    break;
                case "year":
                    movies = movies.OrderBy(m => m.ReleasedYear).ThenBy(m => m.MovieName);
                    break;
                case "genre":
                    movies = movies.OrderBy(m => m.Genre.GenreName).ThenBy(m => m.MovieName);
                    break;
                case "ticket sales":
                    movies = movies.OrderBy(m => m.TicketSales).ThenBy(m => m.MovieName);
                    break;
            }

            // Display Results
            var model = new MovieSearchResults
            {
                Search = search,
                GenreId = genreId,
                MinYear = minYear,
                MaxYear = maxYear,
                SortBy = sortBy,
                Genres = db.Genres.OrderBy(g => g.GenreName).ToList(),
                Results = movies.ToPagedList(page ?? 1, ItemsPerPage ?? 5),
            };

            ViewBag.Search = search;
            ViewBag.GenreId = genreId;
            ViewBag.MinYear = minYear;
            ViewBag.MaxYear = maxYear;
            ViewBag.SortBy = sortBy;
            ViewBag.Genres = db.Genres.OrderBy(g => g.GenreName).ToList();
            return View("Index", model);
        }

        // GET: ScaffoldMovie/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        private void PopulateDropDowns()
        {
            string folderPath = Server.MapPath("~/Content/Images");
            ViewBag.Images =
                Directory.EnumerateFiles(folderPath)
                         .Select(x => Path.GetFileName(x));
        }

        // GET: ScaffoldMovie/Create
        public ActionResult Create()
        {
            ViewBag.GenreId = new SelectList(db.Genres, "GenreId", "GenreName");
            PopulateDropDowns();
            return View();
        }

        // POST: ScaffoldMovie/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MovieId,MovieName,MovieDescription,ReleasedYear,TicketSales,GenreId,Image")]
        Movie movie,
        HttpPostedFileBase image)
        {
            if (image != null && ValidateFile(image))
            {
                SaveMovieImage(movie, image);
            }

            if (ModelState.IsValid)
            {
                db.Movies.Add(movie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GenreId = new SelectList(db.Genres, "GenreId", "GenreName", movie.GenreId);
            return View(movie);
        }
        

        public bool ValidateFile(HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError("myFile", "Please select a file.");
                return false;
            }
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError("myFile", "Invalid File");
                return false;
            }
            if (file.ContentLength >= 5000000)
            {
                ModelState.AddModelError("myFile", "File size was to big.");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!Constants.FILE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError("myFile", $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        public void UploadImage(Movie movie, HttpPostedFileBase file)
        {
            if (file != null)
            {
                if (ValidateFile(file))
                {
                    try
                    {
                        SaveMovieImage(movie, file);
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("myFile", "Sorry error occurred saving file to disk");
                    }
                }
            }
        }

        private void SaveMovieImage(Movie movie, HttpPostedFileBase file)
        {
            //string fileExtension = Path.GetExtension(file.FileName).ToLower();
            //string fileName = $"MOVIE_{movie.MovieId}{fileExtension}";
            string fileName = file.FileName;

            // Resize the image
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > 450 || img.Height > 600)
            {
                img.Resize(450, 600);
            }
            img.Save(Constants.MOVIE_FOLDER + fileName);

            // Save the thumbnails
            img.Resize(120, 100);
            img.Save(Constants.MOVIE_FOLDER + Constants.MOVIE_THUMBNAILS + fileName);

            // Save the name of the image to the database
            movie.Image = fileName;
        }

        // GET: ScaffoldMovie/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            ViewBag.GenreId = new SelectList(db.Genres, "GenreId", "GenreName", movie.GenreId);
            return View(movie);
        }

        // POST: ScaffoldMovie/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MovieId,MovieName,MovieDescription,ReleasedYear,TicketSales,GenreId,Image")]
        Movie movie, 
        HttpPostedFileBase imageUpload)
        {
            if (imageUpload != null &&
                ValidateFile(imageUpload))
            {
                SaveMovieImage(movie, imageUpload);
            }

            if (ModelState.IsValid)
            {
                db.Entry(movie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GenreId = new SelectList(db.Genres, "GenreId", "GenreName", movie.GenreId);
            return View(movie);
        }

        // GET: ScaffoldMovie/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: ScaffoldMovie/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movie movie = db.Movies.Find(id);
            db.Movies.Remove(movie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                db = null;
            }
            base.Dispose(disposing);
        }
    }
}
