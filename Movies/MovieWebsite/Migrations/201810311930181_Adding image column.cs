namespace MovieWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingimagecolumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "Image", c => c.String());
            AlterColumn("dbo.Movies", "MovieDescription", c => c.String(nullable: false, maxLength: 600));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Movies", "MovieDescription", c => c.String(nullable: false));
            DropColumn("dbo.Movies", "Image");
        }
    }
}
