// <auto-generated />
namespace MovieWebsite.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Addingactorstable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Addingactorstable));
        
        string IMigrationMetadata.Id
        {
            get { return "201811011805217_Adding actors table"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
