namespace MovieWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingactortable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actor",
                c => new
                    {
                        ActorId = c.Int(nullable: false, identity: true),
                        ActorName = c.String(),
                    })
                .PrimaryKey(t => t.ActorId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Actor");
        }
    }
}
