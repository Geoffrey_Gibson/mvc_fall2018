namespace MovieWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingimagecolumntoactortable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Actor", "ActorImage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Actor", "ActorImage");
        }
    }
}
