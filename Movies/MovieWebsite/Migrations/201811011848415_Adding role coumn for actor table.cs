namespace MovieWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingrolecoumnforactortable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Actor", "ActorRole", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Actor", "ActorRole");
        }
    }
}
