namespace MovieWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ValidationconstraintsonActorsandNavformovieclass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "ActorId", c => c.Int(nullable: false));
            AlterColumn("dbo.Actor", "ActorName", c => c.String(nullable: false));
            AlterColumn("dbo.Actor", "ActorRole", c => c.String(nullable: false, maxLength: 30));
            CreateIndex("dbo.Movies", "ActorId");
            AddForeignKey("dbo.Movies", "ActorId", "dbo.Actor", "ActorId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Movies", "ActorId", "dbo.Actor");
            DropIndex("dbo.Movies", new[] { "ActorId" });
            AlterColumn("dbo.Actor", "ActorRole", c => c.String());
            AlterColumn("dbo.Actor", "ActorName", c => c.String());
            DropColumn("dbo.Movies", "ActorId");
        }
    }
}
