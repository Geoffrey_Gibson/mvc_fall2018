namespace MovieWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Validationforactorclass : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Actor", "ActorName", c => c.String(nullable: false));
            AlterColumn("dbo.Actor", "ActorRole", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Actor", "ActorRole", c => c.String());
            AlterColumn("dbo.Actor", "ActorName", c => c.String());
        }
    }
}
