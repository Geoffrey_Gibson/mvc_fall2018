namespace MovieWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingnavpropstomovieandactors : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Actor", "MovieId", c => c.Int(nullable: false));
            CreateIndex("dbo.Actor", "MovieId");
            AddForeignKey("dbo.Actor", "MovieId", "dbo.Movies", "MovieId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Actor", "MovieId", "dbo.Movies");
            DropIndex("dbo.Actor", new[] { "MovieId" });
            DropColumn("dbo.Actor", "MovieId");
        }
    }
}
