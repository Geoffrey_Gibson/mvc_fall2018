﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MovieWebsite.Models
{
    [Table("Actor")]
    public class Actor
    {
        [Key]
        public int ActorId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Actor must have a name")]
        public string ActorName { get; set; }

        [MinLength(3, ErrorMessage = "Actor role is too short")]
        [MaxLength(30, ErrorMessage = "Actor role is too long")]
        [Required(ErrorMessage = "Actor must have a role")]
        [Display(Name = "Role")]
        public string ActorRole { get; set; }

        public string ActorImage { get; set; }

        // Navigation
        [Display(Name = "Movie")]
        public int MovieId { get; set; }
        public virtual Movie movie { get; set; }
    }
}