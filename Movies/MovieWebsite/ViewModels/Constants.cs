﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieWebsite.ViewModels
{
    public static class Constants
    {
        // Movie Folders
        public const string MOVIE_FOLDER = "~/Content/MovieImages/";
        public const string MOVIE_THUMBNAILS = "Thumbnails/";

        public const string ACTOR_FOLDER = "~/Content/ActorImages/";
        public const string ACTOR_THUMBNAILS = "Thumbnails/";


        public static readonly string[] FILE_EXTENSIONS = new string[]
        {
            ".jpg", ".jpeg", ".png", ".gif"
        };
    }
}