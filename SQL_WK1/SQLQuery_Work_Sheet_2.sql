use SalesOrders;
--SQL Worksheet 2

--Question 1:
--INSERT INTO customers (
--custFirstName,
--custLastName,
--custStreetAddress,
--custCity,
--custState,
--custZipCode,
--custAreaCode,
--custPhoneNumber)
--VALUES 
--('Evan', 'Gudmestad', '4431 Finney Avenue', 'St. Louis', 'MO', '63113', '314', '286-3691'),
--('Charles', 'Corrigan', '751 Parr Rd.', 'Wentzille', 'MO', '63385', '314', '286-4848'),
--('Jeff', 'Scott', '751 Parr Rd.', 'Wentzille', 'MO', '63385', '314', '286-3675')
--Question 1 End

--Question 2:
--SELECT customerID, custCity, custState, custZipCode 
--FROM customers

--UPDATE customers SET custZipCode = '73301'
--WHERE custState = 'TX' AND custCity = 'Austin';

--UPDATE customers SET custZipCode = '92292'
--WHERE custState = 'CA' AND custCity = 'Palm Springs';

--UPDATE customers SET custZipCode = '91911'
--WHERE custState = 'CA' AND custCity = 'San Diego';

--UPDATE customers SET custZipCode = '98117'
--WHERE custState = 'WA' AND custCity = 'Seattle';
--Question 2 END

-- Question 3:
--SELECT *
--FROM customers c
--LEFT JOIN orders o on c.customerID = o.customerID
--where o.orderNumber is NULL;
--DELETE FROM customers
--WHERE custFirstName = 'Jeff' and custLastName = 'Scott'
--Question 3 END

----Question 4:
--ALTER TABLE employees
--ADD empHireDate date

--SELECT CONCAT(e.empFirstName, ' ', e.empLastName) AS  [Full Name], e.EmpHireDate as [Date Employed]
--FROM employees e

--UPDATE employees SET EmpHireDate = '2010-09-21'
--WHERE empFirstName ='Ann' AND empLastName = 'Patterson'

--UPDATE employees SET EmpHireDate = '2011-11-11'
--WHERE empFirstName ='Mary' AND empLastName = 'Thompson'

--UPDATE employees SET EmpHireDate = '2007-12-13'
--WHERE empFirstName ='Matt' AND empLastName = 'Berg'

--UPDATE employees SET EmpHireDate = '2000-09-19'
--WHERE empFirstName ='Carol' AND empLastName = 'Viescas'

--UPDATE employees SET EmpHireDate = '2000-10-10'
--WHERE empFirstName ='Kirk' AND empLastName = 'DeGrasse'

--UPDATE employees SET EmpHireDate = '2000-09-19'
--WHERE empFirstName ='David' AND empLastName = 'Viescas'

--UPDATE employees SET EmpHireDate = '2015-02-02'
--WHERE empFirstName ='Kathryn' AND empLastName = 'Patterson'

--UPDATE employees SET EmpHireDate = '2017-09-18'
--WHERE empFirstName ='Susan' AND empLastName = 'McLain'
--Question 4 END

--Question 5:
--SELECT 
--CONCAT(e.empFirstName, ' ', e.empLastName) AS [Full Name],
--DATEDIFF(day, e.empHireDate, GETDATE()) AS [Days Employed] 
--FROM employees e
--Question 5 END

--SKIP #6

--Question 7:
--ALTER TABLE product_vendors
--ADD expectedDeliverDate date

--UPDATE product_vendors 
--SET expectedDeliverDate = DATEADD(day, daysToDeliver, GETDATE());

--SELECT *
--FROM product_vendors pv
--Question 7 END

--Question 8:
--SELECT 
--p.productName AS [Product Name],
--p.retailPrice AS [Retail Price],
--od.quantityOrdered AS [Quantity Ordered],
--v.wholesalePrice AS [Wholesale Price]
--FROM products p
--INNER JOIN order_details od ON od.productNumber = p.productNumber
--INNER JOIN product_vendors v ON v.productNumber = p.productNumber
--ORDER BY od.quantityOrdered DESC, p.retailPrice DESC, v.wholesalePrice DESC
--Question 8 END

--Question 9:
--ALTER TABLE products
--ADD UpdatedWholesalePrice 
--SELECT 
--p.productName AS [Product Name],
--p.retailPrice AS [Retail Price],
--od.quantityOrdered AS [Quantity Ordered],
--(CASE 
--	WHEN od.quantityOrdered >= 5 THEN CAST((v.wholesalePrice * .75) AS decimal(20,2))
--	ELSE v.wholesalePrice
--END) AS [Discounted Price],
--v.wholesalePrice AS [Wholesale Price]
--FROM products p
--INNER JOIN order_details od ON od.productNumber = p.productNumber
--INNER JOIN product_vendors v ON v.productNumber = p.productNumber
--ORDER BY od.quantityOrdered DESC, p.retailPrice DESC, v.wholesalePrice DESC
--Question 9 END

--Question 10:
--SELECT 
--c.custFirstName,
--c.custLastName,
--c.custStreetAddress,
--c.custCity, c.custState,
--(CASE c.custState
--	WHEN 'CA' THEN 'California'
--	WHEN 'OR' THEN 'Oregon' 
--	WHEN 'TX' THEN 'Texas' 
--	WHEN 'WA' THEN 'Washington' 
--	ELSE 'Unknown' 
--END) AS StateName
--FROM customers c
--Question 10 END

--Question 11:
--SELECT COUNT(*) AS [Orders Placed]
--FROM customers c
--INNER JOIN orders o on o.customerID = c.customerID
--WHERE c.custFirstName = 'Zachary' 
--AND c.custLastName = 'Ehrlich'
--Question 11 END

--Question 12:
--SELECT COUNT(*) AS [Orders above $50]
--FROM orders o
--INNER JOIN order_details od on od.orderNumber = o.orderNumber
--INNER JOIN products p on p.productNumber = od.productNumber
--WHERE p.retailPrice > 50;
--Question 12 END

--Question 13:
--SELECT 
--od.orderNumber AS [Order Number],
--SUM(od.quotedPrice * od.quantityOrdered) AS OrderTotal
--FROM order_details od
--INNER JOIN orders o on o.orderNumber = od.orderNumber
--INNER JOIN customers c on c.customerID = o.customerID
--WHERE c.custFirstName = 'Zachary' AND c.custLastName = 'Ehrlich'
--GROUP BY od.orderNumber
--HAVING SUM(od.quotedPrice * od.quantityOrdered) > 50
--ORDER BY OrderTotal DESC
--Question 13 END

--Question 14:
--SELECT 
--c.custFirstName AS [First Name],
--c.custLastName AS [Last Name],
--SUM(od.quantityOrdered) AS [Quantity Ordered]
--FROM customers c 
--INNER JOIN orders o on o.customerID = c.customerID
--INNER JOIN order_details od on od.orderNumber = o.orderNumber
--GROUP BY c.custFirstName, c.custLastName
--ORDER BY SUM(od.quantityOrdered) DESC 
--Question 14 END

--Question 15:
--SELECT 
--c.custFirstName AS [First Name],
--c.custLastName AS [Last Name],
--SUM(od.quantityOrdered) AS [Total Quantity Ordered]
--FROM customers c 
--INNER JOIN orders o on o.customerID = c.customerID
--INNER JOIN order_details od on od.orderNumber = o.orderNumber
--GROUP BY c.custFirstName, c.custLastName
--HAVING SUM(od.quantityOrdered) <= 200
--ORDER BY [Total Quantity Ordered] DESC 
-- Question 15 END

--Question 16:
--SELECT 
--v.vendName AS [Vendor Name],
--p.productName AS [Product Name],
--SUM(od.quantityOrdered) AS [Total Quantity Ordered],
--MAX(p.quantityOnHand) AS [Quantity On Hand]
--FROM vendors v
--INNER JOIN product_vendors pv on pv.vendorID = v.vendorID
--INNER JOIN products p on p.productNumber = pv.productNumber
--INNER JOIN order_details od on od.productNumber = p.productNumber
--GROUP BY v.vendName, p.productName
--HAVING SUM(od.quantityOrdered) > MAX(p.quantityOnHand)
--ORDER BY [Vendor Name], [Product Name] DESC
----Question 16 END

----Question 17:
--SELECT 
--v.vendName AS [Vendor Name],
--SUM(od.quantityOrdered) AS [Total Quantity Ordered],
--MAX(p.quantityOnHand) AS [Quantity On Hand]
--FROM vendors v
--INNER JOIN product_vendors pv on pv.vendorID = v.vendorID
--INNER JOIN products p on p.productNumber = pv.productNumber
--INNER JOIN order_details od on od.productNumber = p.productNumber
--GROUP BY p.productName, v.vendName
--HAVING SUM(od.quantityOrdered) > MAX(p.quantityOnHand)
--ORDER BY [Total Quantity Ordered] DESC
--Question 17 END