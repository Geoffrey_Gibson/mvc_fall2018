use SalesOrders;

--Question 1:
insert into customers (
	custFirstName,
	custLastName,
	custStreetAddress,
	custCity,
    custState,
	custZipCode,
	custAreaCode,
	custPhoneNumber)
values('Geoffrey', 'Gibson', '4321 W Belle Pl', 'Saint Louis', 'MO', '63108', '314', '7452263');
--Question 1 End

--Question 2:
select * from customers
where custFirstName <> 'Geoffrey';

select * from customers
where customerID not in(1033); 
--Question 2 End

--Question 3: 
select c.custFirstName, c.custLastName, o.orderNumber, d.productNumber, d.quotedPrice 
from customers c
inner join orders o on c.customerID = o.customerID
inner join order_details d on o.orderNumber = d.orderNumber;
--Question 3 End

--Question 4:
select *
from customers c
where ((c.custFirstName like 'a%' OR
	   c.custFirstName like 'e%' OR
	   c.custFirstName like 'i%' OR
	   c.custFirstName like 'o%' OR
	   c.custFirstName like 'u%') AND
	  (c.custLastName like '%a' OR
	   c.custLastName like '%e' OR
	   c.custLastName like '%i' OR
	   c.custLastName like '%o' OR
	   c.custLastName like '%u')
)
order by c.custLastName, c.custFirstName ASC;
--Question 4 End

--Question  5:
select *
from customers c
where ((c.custFirstName like 'a%' OR
	   c.custFirstName like 'e%' OR
	   c.custFirstName like 'i%' OR
	   c.custFirstName like 'o%' OR
	   c.custFirstName like 'u%') OR
	  (c.custLastName like '%a' OR
	   c.custLastName like '%e' OR
	   c.custLastName like '%i' OR
	   c.custLastName like '%o' OR
	   c.custLastName like '%u')
)
order by c.custLastName, c.custFirstName ASC;
--Question 5 End

--Question 6:
select p.productName, p.retailPrice, c.categoryDescription
from products p
inner join categories c on p.categoryID = c.categoryID
--Question 6 End

--Question 7:
select p.productName, c.categoryDescription, p.retailPrice
from products p
inner join categories c on p.categoryID = c.categoryID
where (
(c.categoryDescription = 'Bikes') OR
(c.categoryDescription = 'Wheels')
)
order by c.categoryDescription ASC, p.retailPrice DESC;
--Question 7 End

--Question 8:
select p.productName, c.categoryDescription, p.retailPrice
from products p
inner join categories c on p.categoryID = c.categoryID
where c.categoryDescription not in('bikes', 'wheels')
order by c.categoryDescription ASC, p.retailPrice DESC;
--Question 8 End

--Question 9:
select p.productName, c.categoryDescription, p.retailPrice, v.vendName, pv.wholesalePrice
from products p
inner join categories c on p.categoryID = c.categoryID
inner join product_vendors pv on p.productNumber = pv.productNumber
inner join vendors v on pv.vendorID = v.vendorID
order by c.categoryDescription ASC, v.vendName DESC, p.retailPrice DESC, pv.wholesalePrice DESC;
--Question 9 End

--Question 10:
select p.productName, c.categoryDescription, p.retailPrice, v.vendName, pv.wholesalePrice
from products p
inner join categories c on p.categoryID = c.categoryID
inner join product_vendors pv on p.productNumber = pv.productNumber
inner join vendors v on pv.vendorID = v.vendorID
where ((v.vendName = 'Lone Star Bike Supply') OR (c.categoryDescription = 'Bikes'))
order by c.categoryDescription ASC, v.vendName DESC, p.retailPrice DESC, pv.wholesalePrice DESC;
--Question 10 End

--Question 11:
select p.productName, c.categoryDescription, p.retailPrice, v.vendName, pv.wholesalePrice
from products p
inner join categories c on p.categoryID = c.categoryID
inner join product_vendors pv on p.productNumber = pv.productNumber
inner join vendors v on pv.vendorID = v.vendorID
where ((v.vendName = 'Lone Star Bike Supply') AND (c.categoryDescription = 'Bikes'))
order by c.categoryDescription ASC, v.vendName DESC, p.retailPrice DESC, pv.wholesalePrice DESC;
--Question 11 End

--Question 12:
select e.empFirstName, e.empLastName, o.orderNumber, d.productNumber, d.quantityOrdered
from employees e
inner join orders o on e.employeeID = o.employeeID
inner join order_details d on o.orderNumber = d.orderNumber
order by d.quantityOrdered DESC;
--Question 12 End

--Question 13:
select e.empFirstName, e.empLastName, o.orderNumber, d.quotedPrice, d.quantityOrdered
from employees e
inner join orders o on e.employeeID = o.employeeID
inner join order_details d on o.orderNumber = d.orderNumber
where d.quantityOrdered > 4
order by d.quantityOrdered DESC;
--Question 13 End

--Question 14:
select e.empFirstName, e.empLastName, o.orderNumber, d.quotedPrice, d.quantityOrdered
from employees e
inner join orders o on e.employeeID = o.employeeID
inner join order_details d on o.orderNumber = d.orderNumber
where d.quantityOrdered <> 4
order by d.quantityOrdered DESC, d.quotedPrice DESC;

select e.empFirstName, e.empLastName, o.orderNumber, d.quotedPrice, d.quantityOrdered
from employees e
inner join orders o on e.employeeID = o.employeeID
inner join order_details d on o.orderNumber = d.orderNumber
where d.quantityOrdered NOT IN (4)
order by d.quantityOrdered DESC, d.quotedPrice DESC;
--Question 14 End

--Question 15:
select c.custFirstName, c.custLastName, o.employeeID, o.orderNumber, d.productNumber, d.quotedPrice
from customers c 
inner join orders o on c.customerID = o.customerID
inner join order_details d on o.orderNumber = d.orderNumber
order by o.employeeID ASC, d.quotedPrice DESC;
--Question 15 End

--Question 16:
select c.customerID, o.employeeID, o.orderNumber, d.productNumber, p.categoryID, v.vendorID
from customers c
inner join orders o on c.customerID = o.customerID
inner join order_details d on o.orderNumber = d.orderNumber
inner join products p on d.productNumber = p.productNumber
inner join product_vendors v on p.productNumber = v.productNumber
order by o.orderNumber;
--Question 16 End
