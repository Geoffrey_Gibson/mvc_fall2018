use SalesOrders;

--QUESTION 1:
SELECT 
MIN(od.quantityOrdered) AS smallestQuantityOrdered,
MAX(od.quantityOrdered) AS largestQuantityOrdered,
AVG(od.quantityOrdered) AS avergaeQuantityOrdered
FROM order_details od
--QUESTION 1 END

--QUESTION 2:
SELECT 
MIN(daysToDeliver) AS leastDaysToDeliver,
MAX(daysToDeliver) AS mostDaysToDeliver,
AVG(daysToDeliver) AS averageDaysToDeliver
FROM product_vendors 
--QUESTION 2 END

--QUESTION 3:
SELECT COUNT(*) as countOfCustomers, MAX(c.custState) AS stateName
FROM customers c
GROUP BY c.custState
ORDER BY countOfCustomers DESC
--QUESTION 3 END

--QUESTION 4
SELECT COUNT(*) as countOfEmployees, MAX(e.empState) AS stateName
FROM employees e
GROUP BY e.empState
ORDER BY countOfEmployees DESC
--QUESTION 4 END

--QUESTION 5
SELECT 
c.categoryDescription as categoryDescription, 
COUNT(p.productNumber) as productCount
FROM categories c
INNER JOIN products p on p.categoryID = c.categoryID
GROUP BY categoryDescription
--QUESTION 5 END

--QUESTION 6
SELECT 
FORMAT(MAX(orderDate), 'MM-dd-yyyy')  as orderDate,
FORMAT(MAX(shipDate), 'MM-dd-yyyy') as shipDate,
MAX(DATEDIFF(day, orderDate, shipDate)) as dayDifference
FROM orders o

SELECT *
FROM  orders o 
WHERE DATEDIFF(day, orderDate, shipDate) = 4
--QUESTION 6 END

--QUESTION 7
SELECT 
FORMAT(MIN(orderDate), 'MM-dd-yyyy')  as orderDate,
FORMAT(MIN(shipDate), 'MM-dd-yyyy') as shipDate,
MIN(DATEDIFF(month, orderDate, shipDate)) as dayDifference
FROM orders o

SELECT * 
FROM orders 
WHERE DATEDIFF(month, orderDate, shipDate) = 0
--QUESTION 7 END

--QUESTION 8
SELECT 
CAST(FORMAT(o.orderDate, 'MM-dd-yyyy') as date) as orderDate,
CAST(FORMAT(o.shipDate, 'MM-dd-yyyy') as date) as shipDate,
od.quantityOrdered AS quantityOrdered,
pv.wholesalePrice AS wholesalePrice,
p.retailPrice AS retailPrice,
od.quotedPrice AS quotedPrice,
c.categoryDescription AS categoryDescription,
v.vendName AS vendorName
FROM orders o
INNER JOIN order_details od on od.orderNumber = o.orderNumber
INNER JOIN products p on p.productNumber = od.productNumber
INNER JOIN product_vendors pv on pv.productNumber = p.productNumber
INNER JOIN categories c on c.categoryID = p.categoryID
INNER JOIN vendors v on v.vendorID = pv.vendorID
WHERE v.vendName LIKE 'a%' AND  quotedPrice > 150
--QUESTION 8 END

--QUESTION 9
SELECT 
c.customerID,
e.employeeID,
CAST(FORMAT(o.shipDate, 'MM-dd-yyyy') as DATE),
pv.wholesalePrice,
p.retailPrice,
od.quotedPrice,
cg.categoryDescription,
pv.vendorID
FROM customers c
INNER JOIN orders o on o.customerID = c.customerID
INNER JOIN employees e on e.employeeID = o.employeeID
INNER JOIN order_details od on od.orderNumber = o.orderNumber
INNER JOIN products p on p.productNumber = od.productNumber
INNER JOIN product_vendors pv on pv.productNumber = p.productNumber
INNER JOIN categories cg on cg.categoryID = p.categoryID
WHERE 
od.quotedPrice != p.retailPrice AND 
pv.wholesalePrice > 125 AND 
pv.wholesalePrice < 130 AND
e.employeeID = '705'
ORDER BY cg.categoryDescription, c.customerID ASC
--QUESTION 9 END