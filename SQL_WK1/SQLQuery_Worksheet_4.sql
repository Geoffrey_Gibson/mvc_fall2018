CREATE PROCEDURE [FindCustomerByID]
	@CustomerID INT
AS
BEGIN
	SELECT 
	c.customerID,
	c.custFirstName,
	c.custLastName,
	c.custStreetAddress,
	c.custCity,
	c.custState,
	c.custPhoneNumber
	FROM customers c
	where c.customerID = @CustomerID
END
GO

EXEC [FindCustomerByID] 1001