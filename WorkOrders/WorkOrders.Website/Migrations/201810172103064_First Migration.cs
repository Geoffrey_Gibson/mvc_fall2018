namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Guid(nullable: false),
                        CustomerName = c.String(nullable: false, maxLength: 50),
                        PhoneNumber = c.String(nullable: false, maxLength: 10),
                        EmailAddress = c.String(maxLength: 200),
                        StreetAddress1 = c.String(maxLength: 200),
                        StreetAddress2 = c.String(maxLength: 10),
                        City = c.String(maxLength: 30),
                        State = c.String(maxLength: 2),
                        ZipCode = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Guid(nullable: false),
                        CustomerId = c.Guid(nullable: false),
                        DateOfRepair = c.DateTime(nullable: false),
                        WrittenBy = c.String(nullable: false),
                        Year = c.Short(nullable: false),
                        Make = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        LicensePlate = c.String(nullable: false),
                        Mileage = c.Int(nullable: false),
                        LUBE = c.Boolean(nullable: false),
                        OIL_CHANGE = c.Boolean(nullable: false),
                        FLUSH_TRANSMISSION = c.Boolean(nullable: false),
                        FLUSH_DIFFERENTIAL = c.Boolean(nullable: false),
                        WASH = c.Boolean(nullable: false),
                        POLISH = c.Boolean(nullable: false),
                        EstimateAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalLaborCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPartCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.OrderId);
            
            CreateTable(
                "dbo.Parts",
                c => new
                    {
                        OrderId = c.Guid(nullable: false),
                        PartNumber = c.String(nullable: false, maxLength: 128),
                        PartName = c.String(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CostPerPart = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsLabor = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.OrderId, t.PartNumber });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Parts");
            DropTable("dbo.Orders");
            DropTable("dbo.Customers");
        }
    }
}
