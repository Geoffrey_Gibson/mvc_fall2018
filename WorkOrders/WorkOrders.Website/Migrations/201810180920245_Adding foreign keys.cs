namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingforeignkeys : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Orders", "OrderId");
            CreateIndex("dbo.Parts", "OrderId");
            AddForeignKey("dbo.Parts", "OrderId", "dbo.Orders", "OrderId", cascadeDelete: true);
            AddForeignKey("dbo.Orders", "OrderId", "dbo.Customers", "CustomerId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "OrderId", "dbo.Customers");
            DropForeignKey("dbo.Parts", "OrderId", "dbo.Orders");
            DropIndex("dbo.Parts", new[] { "OrderId" });
            DropIndex("dbo.Orders", new[] { "OrderId" });
        }
    }
}
