namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Ordercurrencyfieldsnullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "EstimateAmount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "TotalLaborCost", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "TotalPartCost", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "TaxAmount", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "TaxAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "TotalPartCost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "TotalLaborCost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "EstimateAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
