namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingimagecolumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "ImageName", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "ImageName");
        }
    }
}
