namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Removerequiredvalidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "PhoneNumber", c => c.String(nullable: false, maxLength: 14));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "PhoneNumber", c => c.String(nullable: false, maxLength: 10));
        }
    }
}
