namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modifyforeignkey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "OrderId", "dbo.Customers");
            DropColumn("dbo.Orders", "CustomerId");
            RenameColumn(table: "dbo.Orders", name: "OrderId", newName: "CustomerId");
            RenameIndex(table: "dbo.Orders", name: "IX_OrderId", newName: "IX_CustomerId");
            AddForeignKey("dbo.Orders", "CustomerId", "dbo.Customers", "CustomerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.Customers");
            RenameIndex(table: "dbo.Orders", name: "IX_CustomerId", newName: "IX_OrderId");
            RenameColumn(table: "dbo.Orders", name: "CustomerId", newName: "OrderId");
            AddColumn("dbo.Orders", "CustomerId", c => c.Guid(nullable: false));
            AddForeignKey("dbo.Orders", "OrderId", "dbo.Customers", "CustomerId");
        }
    }
}
