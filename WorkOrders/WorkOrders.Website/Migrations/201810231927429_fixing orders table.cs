namespace WorkOrders.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixingorderstable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.Customers");
            RenameColumn(table: "dbo.Orders", name: "CustomerId", newName: "OrderId");
            AddColumn("dbo.Orders", "CustomerId", c => c.Guid(nullable: false));
            AddForeignKey("dbo.Orders", "CustomerId", "dbo.Customers", "CustomerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
        }
    }
}
