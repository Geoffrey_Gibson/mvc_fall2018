﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkOrders.Website.Models
{
    public class Customer
    {
        [Key]
        [Display(Name = "Customer #")]
        public Guid CustomerId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Customer must have a name.")]
        [StringLength(50, ErrorMessage = "Customer name cannot be longer than 50 characters.")]
        [MinLength(3, ErrorMessage = "Name must have at least 3 characters.")]
        public string CustomerName { get; set; }
        
        [Display(Name = "Phone Number (w/ Area Code)")]
        [Required(ErrorMessage = "You must enter a phone number.")]
        [StringLength(14, ErrorMessage = "Phone number cannot exceed 14 characters.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(1-)?(\d{3})|(\(\d{3}\) )-?\d{3}-?\d{4}$", 
         ErrorMessage = "Invalid Format: \n\r (123) 456-7890 \n\r 123-456-7890 \n\r 1234567890 \n\r 1-123-456-7890")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        [StringLength(200, ErrorMessage = "Email address cannot be longer than 200 characters.")]
        [MinLength(6, ErrorMessage = "Email Address must be at least 6 characters.")]
        public string EmailAddress { get; set; }

        [Display(Name = "Street Addres 1")]
        [StringLength(200)]
        [MinLength(10)]
        public string StreetAddress1 { get; set; }

        [Display(Name = "Apt/Suite")]
        [StringLength(10)]
        [MinLength(1)]
        public string StreetAddress2 { get; set; }

        [Display(Name = "City")]
        [StringLength(30)]
        public string City { get; set; }

        [Display(Name = "State")]
        [StringLength(2)]
        [MinLength(2)]
        public string State { get; set; }

        [Display(Name = "Zip Code")]
        [RegularExpression(@"^(\d{4}-)?\d{5}$", ErrorMessage = "Invalid Zip Code")]
        [StringLength(10)]
        [MinLength(5)]
        [DataType(DataType.PostalCode)]
        public string ZipCode { get; set; }

        [Display(Name = "Image")]
        [StringLength(50)]
        [DataType(DataType.ImageUrl)]
        public string ImageName { get; set; }

        // Navigation
        [ForeignKey("CustomerId")]
        public virtual ICollection<Order> Orders { get; set; }
    }
}