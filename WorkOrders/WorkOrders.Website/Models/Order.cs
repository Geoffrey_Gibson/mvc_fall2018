﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkOrders.Website.Models
{
    public class Order
    {
        // General
        [Key]
        public Guid OrderId { get; set; }

        [Display(Name = "Customer")]
        [Required(ErrorMessage = "You must select a customer.")]
        public Guid CustomerId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "You must enter a date of repair.")]
        public DateTime DateOfRepair { get; set; }

        [Display(Name = "Written By")]
        [Required(ErrorMessage = "You must enter your name.")]
        public string WrittenBy { get; set; }

        // Vehicle Info
        [Required(ErrorMessage = "Vehicle must have a year.")]
        public short Year { get; set; }

        [Required(ErrorMessage = "Vehicle must have a make.")]
        [StringLength(20)]
        public string Make { get; set; }

        [Required(ErrorMessage = "Vehicle must have a model.")]
        [StringLength(50)]
        public string Model { get; set; }

        [Display(Name = "License Plate #")]
        [Required(ErrorMessage = "Vehicle must have a license plate.")]
        [StringLength(8)]
        public string LicensePlate { get; set; }

        [Required(ErrorMessage = "Vehicle must have a mileage.")]
        public int Mileage { get; set; }

        // Check Boxes
        public bool LUBE { get; set; }
        public bool OIL_CHANGE { get; set; }
        public bool FLUSH_TRANSMISSION { get; set; }
        public bool FLUSH_DIFFERENTIAL { get; set; }
        public bool WASH { get; set; }
        public bool POLISH { get; set; }

        [Display(Name = "Upload Form")]
        [StringLength(46, ErrorMessage = "File name has to be shorter than 30 characters")]
        [DataType(DataType.Upload)]
        public string FormFileName { get; set; }

        // Calculations
        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "Estimate required.")]
        public decimal? EstimateAmount { get; set; }

        [DataType(DataType.Currency)]
        public decimal? TotalLaborCost { get; set; }

        [DataType(DataType.Currency)]
        public decimal? TotalPartCost { get; set; }

        [DataType(DataType.Currency)]
        public decimal? TaxAmount { get; set; }

        [NotMapped]
        [DataType(DataType.Currency)]
        public decimal? SubTotal
        {
            get { return (TotalLaborCost + TotalPartCost); }
        }
        [NotMapped]
        [DataType(DataType.Currency)]
        public decimal? GrandTotal
        {
            get { return (TotalLaborCost + TotalPartCost + TaxAmount); }
        }

        // Navigation
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("OrderId")]
        public virtual ICollection<Part> Parts { get; set; }
    }
}