﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WorkOrders.Website.Startup))]
namespace WorkOrders.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
