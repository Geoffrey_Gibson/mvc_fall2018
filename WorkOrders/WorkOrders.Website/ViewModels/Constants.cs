﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkOrders.Website.ViewModels
{
    public static class Constants
    {
        public const int MAX_UPLOAD_FILE_SIZE = 5 * 1024 * 1024;
        public const int MAX_UPLOAD_IMAGE_SIZE = 5 * 1024 * 1024;

        public const string THUMBNAILS_FOLDER = "Thumbnails/";
        public const string CUSTOMER_FOLDER_PATH = "~/Content/Customers/";
        public const string ORDER_FOLDER_PATH = "~/Content/Orders/";

        public static readonly string[] ALLOWED_FORM_EXTENSIONS =
        {
            ".pdf", ".jpg", ".jpeg", ".docx", ".xlsx"
        };
        public static readonly string[] ALLOWED_IMAGE_EXTENSIONS =
        {
            ".jpg", ".jpeg", ".png"
        };
    }
}