﻿alter table employees add dept_no char(4) null;
go

update employees
set dept_no = (
    select top(1) dept_no
    from dept_emp
    where dept_emp.emp_no = employees.emp_no
    order by from_date desc
)

select top(1000) * from employees;

/*
select
    e.emp_no,
    (
        select top(1) dept_no
        from dept_emp
        where dept_emp.emp_no = e.emp_no
        order by from_date desc
    ) as dept_no
from employees e
*/