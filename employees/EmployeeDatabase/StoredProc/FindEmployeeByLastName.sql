﻿CREATE PROCEDURE [dbo].[FindEmployeeByLastName]
	@LastName VARCHAR(16)
AS
BEGIN
	SELECT
		e.emp_no,
		e.first_name,
		e.last_name,
		e.gender,
		e.birth_date
	FROM employees e
	WHERE e.last_name = @LastName
END
GO
