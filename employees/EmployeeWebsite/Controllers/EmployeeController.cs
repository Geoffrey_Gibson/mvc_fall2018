﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using EmployeeWebsite.Models;
using EmployeeWebsite.ViewModels;
using PagedList;

namespace EmployeeWebsite.Controllers
{
    public class EmployeeController : Controller
    {
        private EmployeeDatabase db = new EmployeeDatabase();

        // GET: ScaffoldEmployee
        public ActionResult Index(
            string search,
            string dept_no,
            int? minYear,
            int? maxYear,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            var emps =
                from e in db.employees
                join d in db.departments on e.dept_no equals d.dept_no
                //where e.last_name == "Lenart" // Testing
                orderby e.emp_no, e.dept_no
                select new EmployeeInfo
                {
                    emp_no = e.emp_no,
                    last_name = e.last_name,
                    first_name = e.first_name,
                    hire_date = e.hire_date,
                    dept_no = e.dept_no,
                    dept_name = d.dept_name,
                    emp_image = e.emp_image
                };
            // Search
            if (!string.IsNullOrEmpty(search))
            {
                emps = emps.Where(x => x.dept_name == search || x.last_name == search || x.first_name == search);
            }
            if (dept_no != null)
            {
                emps = emps.Where(x => x.dept_no == dept_no);
            }

            // Sorting
            switch (sortBy)
            {
                default:
                    emps = emps.OrderBy(e => e.emp_no);
                    break;
                case "Employee #":
                    emps = emps.OrderBy(e => e.emp_no);
                    break;
                case "Last Name":
                    emps = emps.OrderBy(e => e.last_name);
                    break;
                case "First Name":
                    emps = emps.OrderBy(e => e.first_name);
                    break;
                case "Hire Date":
                    emps = emps.OrderBy(e => e.hire_date);
                    break;
                case "Department":
                    emps = emps.OrderBy(e => e.dept_name);
                    break;
            }

            var model = new EmployeeSearchResults
            {
                Search = search,
                Dept_no = dept_no,
                MinYear = minYear,
                MaxYear = maxYear,
                SortBy = sortBy,
                Departments = db.departments.OrderBy(d => d.dept_name).ToList(),
                Results = emps.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };

            ViewBag.Search = search;

            return View("Index", model);
        }

        // GET: ScaffoldEmployee/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: ScaffoldEmployee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ScaffoldEmployee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "emp_no,birth_date,first_name,last_name,gender,hire_date,dept_no,emp_image")]
        employee employee,
        HttpPostedFileBase imageUpload)
        {
            if (imageUpload != null && ValidateImage(imageUpload))
            {
                SaveEmpImage(imageUpload, employee);
            }

            if (ModelState.IsValid)
            {
                db.employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: ScaffoldEmployee/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: ScaffoldEmployee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "emp_no,birth_date,first_name,last_name,gender,hire_date,dept_no,emp_image")]
        employee employee,
        HttpPostedFileBase imageUpload)
        {
            if (imageUpload != null && ValidateImage(imageUpload))
            {
                SaveEmpImage(imageUpload, employee);
            }

            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        public bool ValidateImage(HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError("myFile", "Please select a file");
                return false;
            }
            else if (file.ContentLength <= 0)
            {
                ModelState.AddModelError("myFile", "File invalid");
                return false;
            }
            else if (file.ContentLength > Constants.MAX_UPLOAD_SIZE)
            {
                ModelState.AddModelError("myFile", "File too big");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!Constants.FILE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError("myFile", $"{fileExtension} is not a valid file extension please use another");
                return false;
            }
            return true;
        }

        public void SaveEmpImage(HttpPostedFileBase image, employee employee)
        {
            string fileName = image.FileName;

            WebImage img = new WebImage(image.InputStream);
            if (img.Width > 300 || img.Height > 450)
            {
                img.Resize(300, 450);
            }
            img.Save(Constants.EMP_IMAGES + fileName);

            // Image Resize
            img.Resize(100, 100);
            img.Save(Constants.EMP_IMAGES + Constants.THUMBNAILS + fileName);

            // Save Image
            employee.emp_image = fileName;
        }

        // GET: ScaffoldEmployee/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: ScaffoldEmployee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            employee employee = db.employees.Find(id);
            db.employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
