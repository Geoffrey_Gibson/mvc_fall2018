﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeeWebsite.Models;

namespace EmployeeWebsite.Controllers
{
    public class HomeController : Controller
    {
        private EmployeeDatabase db = new EmployeeDatabase();

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                db.Dispose();
                db = null;
            }
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            var departments =
                from d in db.departments
                orderby d.dept_name
                select d;
        
            return View("Index", departments.ToList());
        }
    }
}