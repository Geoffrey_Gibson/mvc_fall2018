﻿using System.ComponentModel.DataAnnotations;

namespace EmployeeWebsite.Models.Extensions
{
    [MetadataType(typeof(department_metadata))]
    public partial class department
    {
    }

    public class department_metadata
    {
        [Display(Name = "Department #")]
        public string dept_no { get; set; }
        [Display(Name = "Department Name")]
        public string dept_name { get; set; }
    }
}