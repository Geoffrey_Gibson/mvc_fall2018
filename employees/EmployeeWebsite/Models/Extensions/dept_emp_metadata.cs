﻿using System.ComponentModel.DataAnnotations;

namespace EmployeeWebsite.Models
{
    [MetadataType(typeof(dept_emp_metadata))]
    public partial class dept_emp
    {
    }

    public class dept_emp_metadata
    {
        [Display(Name = "Employee #")]
        public int emp_no { get; set; }
        [Display(Name = "Department #")]
        public string dept_no { get; set; }
        [Display(Name = "From Date")]
        public System.DateTime from_date { get; set; }
        [Display(Name = "To Date")]
        public System.DateTime to_date { get; set; }
    }
}