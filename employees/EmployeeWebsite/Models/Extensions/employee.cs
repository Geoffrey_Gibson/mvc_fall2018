﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeWebsite.Models
{
    [MetadataType(typeof(employee_metadata))]
    public partial class employee
    {
    }
    
    public partial class employee_metadata
    {
        [Display(Name = "Employee #")]
        public int emp_no { get; set; }
        [Required(ErrorMessage = "Birth date is required.")]
        [Display(Name = "Birth Date")]
        public DateTime birth_date { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First name is required.")]
        public string first_name { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required.")]
        public string last_name { get; set; }
        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Gender is required.")]
        [MinLength(1)]
        public string gender { get; set; }
        [Display(Name = "Hire Date")]
        [Required(ErrorMessage = "Hire date is required.")]
        public DateTime hire_date { get; set; }
        public string emp_image { get; set; }

        public string dept_no { get; set; }

        //[NotMapped]
        //public string full_name
        //{
        //    get { return first_name + " " + last_name; }
        //}
    }
}