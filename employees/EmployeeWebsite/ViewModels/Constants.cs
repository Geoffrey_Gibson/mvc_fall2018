﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeWebsite.ViewModels
{
    public static class Constants
    {
        public const string EMP_IMAGES = "~/Content/Emp_Images/";
        public const string THUMBNAILS = "Thumbnails/";

        public const int MAX_UPLOAD_SIZE = 5 * 1024 * 1024;

        public static readonly string[] FILE_EXTENSIONS = new string[]
        {
            ".jpg", ".jpeg", ".png", ".gif"
        };
    }
}