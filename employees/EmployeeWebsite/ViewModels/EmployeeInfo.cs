﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EmployeeWebsite.Models;
using PagedList;


namespace EmployeeWebsite.ViewModels
{
    public class EmployeeInfo
    {
        [Display(Name = "Employee #")]
        public int emp_no { get; set; }
        [Display(Name = "Last Name")]
        public string last_name { get; set; }
        [Display(Name = "First Name")]
        public string first_name { get; set; }
        [Display(Name = "Hire Date")]
        public DateTime hire_date { get; set; }
        [Display(Name = "Department #")]
        public string dept_no { get; set; }
        [Display(Name = "Department Name")]
        public string dept_name { get; set; }
        public string emp_image { get; set; }
    }
}