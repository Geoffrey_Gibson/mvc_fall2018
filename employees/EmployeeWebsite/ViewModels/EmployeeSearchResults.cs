﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EmployeeWebsite.Models;
using PagedList;

namespace EmployeeWebsite.ViewModels
{
    public class EmployeeSearchResults
    {
        public string Search { get; set; }
        public string Dept_no { get; set; }
        public int? MinYear { get; set; }
        public int? MaxYear { get; set; }
        public string SortBy { get; set; }
        public IEnumerable<department> Departments { get; set; }
        public IPagedList<EmployeeInfo> Results { get; set; }
    }
}